/*
 Navicat Premium Data Transfer

 Source Server         : chatGPT
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : 124.221.21.212:3306
 Source Schema         : chatgpt3

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 25/04/2023 09:57:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `chat_id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '对话id',
  `username` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `user_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户api-key',
  `model` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '使用的model类型',
  `prompt` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '问题',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '答案',
  `is_end` int NOT NULL COMMENT '是否结束',
  `request_ip_address` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ip地址',
  `response_json` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回的responseJSON串',
  `prompt_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finish_reason` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问答结束的原因',
  `request_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求的url',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '请求创建的时间',
  `request_time` int NULL DEFAULT NULL COMMENT '请求完成的时间',
  `max_tokens` int NULL DEFAULT NULL COMMENT '要生成的最大字符数',
  `temperature` double NULL DEFAULT NULL COMMENT '采样温度',
  `thread_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '运行的线程名称',
  PRIMARY KEY (`chat_id`) USING BTREE,
  UNIQUE INDEX `chatid_index`(`chat_id`) USING BTREE,
  UNIQUE INDEX `create_date_index`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2855 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
