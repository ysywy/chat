package com.pomelo.chat.service;

import com.pomelo.chat.domain.Chat;
import com.pomelo.chat.domain.ChatVo;

import java.util.List;

public interface ChatService{


    int deleteByPrimaryKey(Integer chatId);

    int insert(Chat record);

    int insertSelective(Chat record);

    Chat selectByPrimaryKey(Integer chatId);

    int updateByPrimaryKeySelective(Chat record);

    int updateByPrimaryKey(Chat record);

    List<Chat> getAllByUsername(String username);

    List<ChatVo> getPage(String username, Integer pageSize, Integer pageNum);

}
