package com.pomelo.chat.service;


import com.pomelo.chat.domain.Chat;
import com.pomelo.chat.domain.ChatVo;
import com.pomelo.chat.util.Question;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;

public interface ChatGPTService {

    Chat askGPT3(Question question, String ip);

    ChatVo askChatGPT(Question question, String ip) throws ParseException;

    SseEmitter askChatGPT35(String question, HttpServletRequest request) throws IOException;
}
