package com.pomelo.chat.util;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/**
 * 工具类
 */
public class Common {

    public static String getIpAddress(HttpServletRequest request) {
        if (request.getHeader("X-Forwarded-For") != null) {
            return request.getHeader("X-Forwarded-For");
        } else if (request.getHeader("X-Real-IP") != null) {
            return request.getHeader("X-Real-IP");
        } else {
            return "0:0:0:0:0:0:0:1".equals(request.getRemoteAddr()) ? "127.0.0.1" : request.getRemoteAddr();
        }
    }
}
