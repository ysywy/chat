package com.pomelo.chat;

import com.alibaba.fastjson.JSON;
import com.pomelo.chat.config.ChatConfig;
import com.pomelo.chat.config.ChatGPTConfig;
import com.pomelo.chat.domain.Chat;
import com.pomelo.chat.service.ChatGPTService;
import com.pomelo.chat.service.ChatService;
import com.theokanning.openai.OpenAiService;
import com.theokanning.openai.completion.CompletionChoice;
import com.theokanning.openai.completion.CompletionRequest;
import com.theokanning.openai.completion.CompletionResult;
import com.unfbx.chatgpt.OpenAiClient;
import com.unfbx.chatgpt.entity.completions.CompletionResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.List;

@SpringBootTest
class ChatApplicationTests {

    @Resource
    ChatService chatService;

    @Resource
    ChatGPTService chatGPTService;

    @Resource
    private ChatConfig chatConfig;

    @Test
    void contextLoads() {
        Chat chat = chatService.selectByPrimaryKey(235);
        String responseJson = chat.getResponseJson();
        System.out.println(responseJson);
        CompletionResult result = JSON.parseObject(responseJson, CompletionResult.class);
        System.out.println(result.getId());
    }


    @Test
    void testAsk2() {
        OpenAiService openAiService = new OpenAiService(chatConfig.getToken(), Duration.ofSeconds(chatConfig.getTimeOut()));
        ChatGPTConfig chatGPTConfig = chatConfig.getChatGPT();
        long start = System.currentTimeMillis();
        CompletionRequest completionRequest = CompletionRequest.builder()
                .prompt("写一篇关于友情的英文作文")
                .model(chatGPTConfig.getModel())
                .temperature(chatGPTConfig.getTemperature())
                .maxTokens(chatGPTConfig.getMaxTokens())
                .topP(Double.valueOf(chatGPTConfig.getTopP()))
                .echo(chatGPTConfig.getEcho())
                .build();

        CompletionResult result = openAiService.createCompletion(completionRequest);
        long end = System.currentTimeMillis();
        List<CompletionChoice> choices = result.getChoices();
        System.out.println(end - start);
        System.out.println(choices.get(0).getText());
    }

}
