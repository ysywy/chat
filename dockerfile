FROM openjdk:17
COPY target/*.jar /app.jar
CMD ["--server.port=8182"]
EXPOSE 8182
ENTRYPOINT ["java", "-jar", "/app.jar"]